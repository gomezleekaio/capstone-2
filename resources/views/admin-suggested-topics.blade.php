@extends('templates.template')

@section('title', 'Admin - Topics')

@section('side-nav')
    @include('shared.admin-side-nav')
@endsection

@section('content')
    <div class="row">
        @foreach ($topics as $topic)
            <div class="col l4">
                <div class="card topic">
                    <div class="card-image">
                        <img src="{{asset($topic->image)}}" class="img-height">
                    </div>
                    <div class="card-content">
                        <span class="card-title">
                            <strong>{{$topic->name}}</strong>
                        </span>
                        <p>
                            {{$topic->description}}
                        </p>
                    </div>
                    <div class="card-action d-flex">
                        <form action="/admin/approve-topic/{{$topic->id}}" method="POST">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-flat orange-text hoverable">Approve</button>
                        </form>
                        <form action="/admin/delete-topic/{{$topic->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-flat orange-text hoverable">Reject</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
        
    </div>
    
@endsection