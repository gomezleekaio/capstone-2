
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
        <title>@yield('title')</title>
    
        <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    </head>
    <body>
        <header>
            <div class="navbar-fixed">
                <nav class="grey darken-4">
                    <div class="nav-wrapper">
                        <a href="/login" class="brand-logo">Resource Hub</a>
                    </div>
                </nav>
            </div>
        </header>
        
        <main>
            <div class="row">
                <div class="col l12">
                    @yield('content')
                </div>
            </div>
        </main>
        
        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    
        <script src="{{ asset('js/script.js') }}"></script>
    </body>
    </html>