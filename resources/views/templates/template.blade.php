
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Compiled and minified CSS -->
    {{-- materialize --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    {{-- font-awesome --}}
    <script src="https://kit.fontawesome.com/4fa5974cf4.js" crossorigin="anonymous"></script>

</head>
<body>
    <header class="p-left-300">
        <div class="navbar-fixed">
            <nav class="grey darken-4">
                <div class="nav-wrapper">
                    @if(Auth::user()->role_id == 1)
                        <a href="/admin/topics" class="brand-logo">Resource Hub</a>
                    @else
                    <a href="/home" class="brand-logo">Resource Hub</a>
                    @endif
                </div>
            </nav>
        </div>
    </header>

    @yield('side-nav')
    
    <main class="p-left-300">
        <div class="row">
            <div class="col l12">
                @yield('content')
            </div>
        </div>
    </main>
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <script src="{{ asset('js/script.js') }}"></script>

    @yield('script-tags')
</body>
</html>