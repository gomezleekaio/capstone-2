@extends('templates.template')

@section('title', 'Edit Topic')

@section('side-nav')
    @include('shared.admin-side-nav')
@endsection

@section('content')
    <div class="row">
        <div class="col l8">
            <div class="card-panel grey lighten-5" style="margin-top: 70px;">
                <h4 class="center-align">Edit Topic</h4>
                <div class="row">
                    <form action="/admin/edit-topic/{{$topic->id}}" method="POST" class="col l12" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH');
                        <div class="input-field">
                            <i class="small material-icons prefix">language</i>
                            <input type="text" name="topic-name" class="validate" value="{{$topic->name}}">
                            <label for="topic-name">Topic Name</label>
                        </div>
                        <div class="input-field">
                            <i class="small material-icons prefix">comment</i>
                            <textarea name="topic-description" class="materialize-textarea">{{$topic->description}}</textarea>
                            <label for="topic-description">Topic Description</label>
                        </div>
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>File</span>
                                <input type="file" name="image">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                        <div class="input-field center-align">
                            <button type="submit" class="btn-large waves-effect waves-light">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection