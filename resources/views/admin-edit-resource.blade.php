@extends('templates.template')

@section('title', 'Add Resource')

@section('side-nav')
    @php
        $user = Auth::user();
    @endphp
    @include('shared.admin-side-nav')
@endsection

@section('content')
    <div class="row">
        <div class="col l8">
            <div class="card-panel grey lighten-5" style="margin-top: 70px;">
                <div class="row">
                    <form action="/admin/edit-resource/{{$resource->id}}" method="POST" class="col l12">
                        @csrf
                        @method('PATCH')
                        <div class="input-field">
                            <i class="small material-icons prefix">language</i>
                            <input type="text" name="title" class="validate" value="{{$resource->title}}" required>
                            <label for="title">Resource Title</label>
                        </div>
                        <div class="input-field">
                            <i class="small material-icons prefix">comment</i>
                            <input type="url" name="url" class="validate" value="{{$resource->url}}" required>
                            <label for="url">Resource URL</label>
                        </div>
                        <div class="input-field center-align">
                            <button type="submit" class="btn-large waves-effect waves-red green">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection