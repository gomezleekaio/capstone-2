@extends('templates.template')

@section('title', 'Home')

@section('side-nav')
    @include('shared.user-side-nav')
@endsection

@section('content')
    <div class="row">
        <div class="col l4 offset-l3">
            <div class="card topic hoverable">
                <div class="card-image">
                    <img src="{{asset($topic->image)}}" class="img-height">
                </div>
                <div class="card-content">
                    <span class="card-title">
                        <strong>{{$topic->name}}</strong>
                    </span>
                    <p>
                        {{$topic->description}}
                    </p>
                </div>
                <div class="card-action">
                    <div class="d-flex justify-content-space-around">
                        @if(!$topic->pinned())
                            <form method="POST" onsubmit="pinTopic(event)">
                                @csrf
                                <input type="hidden" value="{{$topic->id}}">
                                <button type="submit" class="btn blue hoverable">Pin Topic</button>
                            </form>
                        @else
                            <form method="POST" onsubmit="unpinTopic(event)">
                                @csrf
                                <input type="hidden" value="{{$topic->id}}">
                                <button type="submit" class="btn blue hoverable">Unpin Topic</button>
                            </form>
                        @endif
                            <a href="/submit-resource/{{$topic->id}}" class="btn green hoverable">Submit Resource</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @foreach ($resources as $resource)
        <div class="col l10 offset-l1">
            <div class="card">
                <div class="card-content">
                    <span class="card-title">
                        <a href="{{$resource->url}}">{{$resource->title}}</a>
                    </span>
                    <p class="resource-options">
                        <span class="resource-option">{{$resource->vote_count}} points</span>
                        <span class="resource-option">by {{$resource->user->firstname ." ". $resource->user->lastname}}</span>
                        <span class="resource-option">{{'date'}}</span>
                        <span class="resource-option">
                            <a href="#">
                                <i class="fa fa-comments fa-fw"></i>
                                {{$resource->comment_count}} comments
                            </a>
                        </span>
                    </p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    
@endsection

@section('script-tags')
    <script src="{{asset('js/pin-topic.js')}}"></script>
@endsection