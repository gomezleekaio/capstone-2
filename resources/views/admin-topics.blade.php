@extends('templates.template')

@section('title', 'Admin - Topics')

@section('side-nav')
    @include('shared.admin-side-nav')
@endsection

@section('content')
    <div class="row">
        @foreach ($topics as $topic)
            <div class="col l3">
                <div class="card topic hoverable">
                    <div class="card-image">
                        <img src="{{asset($topic->image)}}" class="img-height">
                        <a 
                            href="/admin/add-resource/{{$topic->id}}"
                            class="btn-floating halfway-fab waves-effect waves-light red">
                            <i class="material-icons">add</i>
                        </a>
                    </div>
                    <div class="card-content">
                        <span class="card-title">
                            <strong>{{$topic->name}}</strong>
                        </span>
                        <p>
                            {{$topic->description}}
                        </p>
                    </div>
                    <div class="card-action">
                        <div class="d-flex justify-content-space-around">
                                <a href="/admin/edit-topic/{{$topic->id}}" class="btn-flat orange-text">Edit Topic</a>
                                <form action="/admin/delete-topic/{{$topic->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn-flat orange-text">Delete Topic</button>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        
    </div>
    
@endsection