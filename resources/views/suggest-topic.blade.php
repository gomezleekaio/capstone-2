@extends('templates.template')

@section('title', 'Add Topic')

@section('side-nav')
    @include('shared.user-side-nav')
@endsection

@section('content')
    <div class="row">
        <div class="col l8">
            <div class="card-panel grey lighten-5" style="margin-top: 70px;">
                <h4 class="center-align">Suggest Topic</h4>
                <div class="row">
                    <form action="/suggest-topic" method="POST" class="col l12" onsubmit="suggestTopic(event)" enctype="multipart/form-data">
                        @csrf
                        <div class="input-field">
                            <i class="small material-icons prefix">language</i>
                            <input type="text" id="topic-name" name="topic-name" class="validate" required>
                            <label for="topic-name">Topic Name</label>
                        </div>
                        <div class="input-field">
                            <i class="small material-icons prefix">comment</i>
                            <textarea name="topic-description" id="topic-description" class="materialize-textarea" required></textarea>
                            <label for="topic-description">Topic Description</label>
                        </div>
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>File</span>
                                <input type="file" name="image" required>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                        <div class="input-field center-align">
                            <button type="submit" class="btn-large waves-effect waves-light">ADD</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection


@section('script-tags')
    <script src="{{asset('js/suggest-topic.js')}}"></script>
@endsection