@extends('templates.template')

@section('title', 'Submit Resource')

@section('side-nav')
    @include('shared.user-side-nav')
@endsection

@section('content')
    <div class="row">
        <div class="col l8">
            <div class="card-panel grey lighten-5" style="margin-top: 70px;">
                <h4 class="center-align">{{$topic->name}}</h4>
                <div class="row">
                    <form action="/admin/add-resource/{{$topic->id}}" method="POST" class="col l12">
                        @csrf
                        <div class="input-field">
                            <i class="small material-icons prefix">language</i>
                            <input type="text" name="title" class="validate">
                            <label for="title">Resource Title</label>
                        </div>
                        <div class="input-field">
                            <i class="small material-icons prefix">comment</i>
                            <input type="text" name="url" class="validate">
                            <label for="url">Resource URL</label>
                        </div>
                        <div class="input-field center-align">
                            <button type="submit" class="btn-large waves-effect waves-red">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection