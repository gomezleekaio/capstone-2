@extends('templates.template')

@section('title', 'Home')

@section('side-nav')
    @include('shared.user-side-nav')
@endsection

@section('content')
    <div class="row m-top-100">
        <div class="col l5 offset-l3">
            <form action="#" class="d-flex" onsubmit="searchTopic(event)">
                @csrf
                <input type="text" id="topic" name="topic" placeholder="Enter a Topic" class="validate" />
                <button type="submit" class="btn">Search</button>
            </form>
        </div>
    </div>
    <div class="row" id="topic-list">
        @foreach($topics as $topic)
            <div class="col l4">
                <div class="card topic hoverable">
                    <div class="card-image">
                        <img src="{{asset($topic->image)}}" class="img-height">
                    </div>
                    <div class="card-content">
                        <span class="card-title">
                            <a href="/show-topic/{{$topic->id}}"><strong>{{$topic->name}}</strong></a>
                        </span>
                        <p>
                            {{$topic->description}}
                        </p>
                    </div>
                    
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('script-tags')
    <script src="{{asset('js/pin-topic.js')}}"></script>
    <script src="{{asset('js/search-topic.js')}}"></script>
@endsection