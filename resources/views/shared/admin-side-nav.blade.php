@php
    $user = Auth::user();
@endphp
<ul class="sidenav sidenav-fixed">
    <li>
        <div class="user-view">
            <div class="background">
                <img src="{{ asset('/images/topics/react.jpeg') }}">
            </div>
            <a href="#user">
                <img class="circle" src="{{ asset($user->image) }}">
            </a>
            <a href="#name"><span class="white-text name">{{$user->firstname . " " . $user->lastname}}</span></a>
        </div>
    </li>
    <li>
        <ul class="collapsible expandable">
            <li>
                <div class="collapsible-header">
                    <i class="small material-icons prefix">dashboard</i>
                    <a href="#">
                        Dashboard
                    </a>
                </div>
            </li>
            <li>
                <div class="collapsible-header">
                    <i class="small material-icons prefix">language</i>
                    Topic
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li>
                            <a href="/admin/topics"class="waves-effect">
                                <i class="small material-icons prefix">format_list_bulleted</i>
                                Topics
                            </a>
                        </li>
                        <li>
                            <a href="/admin/add-topic"class="waves-effect">
                                <i class="small material-icons prefix">add</i>
                                Add Topic
                            </a>
                        </li>
                        <li>
                            <a href="/admin/suggested-topics"class="waves-effect">
                                <i class="small material-icons prefix">format_list_bulleted</i>
                                Suggested Topics
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="collapsible-header">
                    <i class="small material-icons prefix">language</i>
                    Resource
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li>
                            <a href="/admin/resources"class="waves-effect">
                                <i class="small material-icons prefix">format_list_bulleted</i>
                                Resources
                            </a>
                        </li>
                        <li>
                            <a href="/admin/submitted-resources" class="waves-effect">
                                <i class="small material-icons prefix">format_list_bulleted</i>
                                Submitted Resources
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="collapsible-header">
                    <i class="small material-icons prefix">power_settings_new</i>
                    <form action="/logout" method="POST">
                        @csrf
                        <button type="submit" class="black-text btn-flat">Logout</button>
                    </form>
                </div>
            </li>
        </ul>
    </li>
</ul>