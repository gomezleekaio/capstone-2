@php
    $user = Auth::user();
@endphp
<ul class="sidenav sidenav-fixed">
    <li>
        <div class="user-view">
            <div class="background">
                <img src="{{ asset('/images/topics/react.jpeg') }}">
            </div>
            <a href="#user">
                <img class="circle" src="{{ asset($user->image) }}">
            </a>
            <a href="#">
                <span class="white-text name">{{$user->firstname . " " . $user->lastname}}</span>
            </a>
        </div>
    </li>
    <li>
        <ul class="collapsible expandable">
            <li>
                <a href="/home" class="waves-effect collapsible-header">
                    <i class="small material-icons prefix">home</i>
                    Home
                </a>
            </li>
            <li>
                <div class="collapsible-header">
                    <i class="small material-icons prefix">language</i>
                    Pinned Topics
                </div>
                <div class="collapsible-body">
                    <ul id="pinned-topics-list">
                        @foreach ($user->topics as $topic)
                            <li>
                                <a href="/show-topic/{{$topic->id}}"class="waves-effect">
                                    <i class="small material-icons prefix">format_list_bulleted</i>
                                    {{$topic->name}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </li>
            <li>
                <a href="/suggest-topic" class="collapsible-header">
                    <i class="small material-icons prefix">power_settings_new</i>
                    Suggest Topic
                </a>
            </li>
            <li>
                <form action="/logout" class="collapsible-header" method="POST">
                    <i class="small material-icons prefix">power_settings_new</i>
                    @csrf
                    <button type="submit" class="black-text btn-flat">Logout</button>
                </form>
            </li>
        </ul>
    </li>
</ul>