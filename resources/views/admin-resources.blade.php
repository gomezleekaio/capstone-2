@extends('templates.template')

@section('title', 'Resources')

@section('side-nav')
    @include('shared.admin-side-nav')
@endsection

@section('content')
    <div class="row">
        <div class="col l12">
            <table>
                <thead>
                    <tr>
                        <th>Resource Title</th>
                        <th>Resource Url</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($resources as $resource)
                        <tr>
                            <td><strong>{{$resource->title}}</strong></td>
                            <td><a href="{{$resource->url}}">{{$resource->url}}</a></td>
                            <td class="d-flex justify-content-space-around">
                                <a href="/admin/edit-resource/{{$resource->id}}" class="btn green">Edit</a>
                                <form action="/admin/delete-resource/{{$resource->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn red">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection