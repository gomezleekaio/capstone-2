<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

// ---------- User ------------
Route::post('/pin-topic', 'TopicController@pinTopic');
Route::post('/unpin-topic', 'TopicController@unpinTopic');
Route::get('/submit-resource/{id}', 'ResourceController@showSubmitResourceForm');
Route::post('/submit-resource/{id}', 'ResourceController@store');

// topic
Route::get('/show-topic/{id}', 'TopicController@showTopic');

Route::post('/search-topic', 'TopicController@searchTopic');

Route::get('/suggest-topic', 'TopicController@showSuggestTopicForm');
Route::post('/suggest-topic', 'TopicController@suggestTopic');



// ---------- Admin ------------
// topics
Route::get('/admin/topics', 'TopicController@listTopics');
Route::get('/admin/add-topic', 'TopicController@create');
Route::post('/admin/add-topic', 'TopicController@store');
Route::get('/admin/edit-topic/{id}', 'TopicController@edit');
Route::patch('/admin/edit-topic/{id}', 'TopicController@update');
Route::delete('/admin/delete-topic/{id}', 'TopicController@destroy');

// suggested topics
Route::get('/admin/suggested-topics', 'TopicController@listSuggestedTopics');
Route::patch('/admin/approve-topic/{id}', 'TopicController@approveTopic');

// resources
Route::get('/admin/resources', 'ResourceController@listResources');
Route::get('/admin/add-resource/{id}', 'ResourceController@create');
Route::post('/admin/add-resource/{id}', 'ResourceController@store');
Route::get('/admin/edit-resource/{id}', 'ResourceController@edit');
Route::patch('/admin/edit-resource/{id}', 'ResourceController@update');
Route::delete('/admin/delete-resource/{id}', 'ResourceController@destroy');
Route::patch('/admin/approve-resource/{id}', 'ResourceController@approveResource');

// submitted resources
Route::get('/admin/submitted-resources', 'ResourceController@listSubmittedResources');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/logout', 'Auth\LoginController@logout');
