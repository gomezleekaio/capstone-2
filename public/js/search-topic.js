
const searchTopic = (event) => {
    event.preventDefault();
    topic = document.querySelector('#topic').value;

    let data = new FormData;

    data.append('_token', "{{csrf_token()}}");
    data.append('topic', topic);
    
    fetch('/search-topic', {
        method: 'POST',
        credentials: 'same-origin',
        mode: 'cors',
        body: data
    }).then(response => {
        return response.json();
    }).then(data => {
        clearTopicList();
        listTopics(data);
    });
}

const clearTopicList = () => {
    document.querySelector('#topic-list').innerHTML = '';
}

const createTopic = (topic) => {
    let newTopic = `
    <div class="col l4">
        <div class="card topic hoverable">
            <div class="card-image">
                <img src="../${topic.image}" class="img-height">
            </div>
            <div class="card-content">
                <span class="card-title">
                    <a href="/show-topic/${topic.id}"><strong>${topic.name}</strong></a>
                </span>
                <p>
                    ${topic.description}
                </p>
            </div>
            
        </div>
    </div>
    `;
    return newTopic;
}

const listTopics = (topics) => {
    topics.forEach(topic => {
        let topicList = document.querySelector('#topic-list');
        topicList.innerHTML += createTopic(topic);
    });
}