

const pinTopic = (event) => {
    event.preventDefault();
    let id = event.target.firstChild.nextElementSibling.nextElementSibling.value;
    let parentDiv = event.target.parentNode;
    let data = new FormData;

    data.append('_token', "{{csrf_token()}}");
    data.append('id', id);
    
    fetch('/pin-topic', {
        method: 'POST',
        credentials: 'same-origin',
        mode: 'cors',
        body: data
    }).then(response => {
        return response.json();
    }).then(data => {
        appendTopic(data);
        parentDiv.innerHTML = '';
        parentDiv.innerHTML += `
        <form method="POST" onsubmit="unpinTopic(event)">
            <input type="hidden" value="{{csrf_token()}}" />
            <input type="hidden" name="id" value="${id}" />
            <button type="submit" class="btn blue hoverable">Unpin Topic</button>
        </form>
        <a href="/submit-resource/${id}" class="btn green hoverable">Submit Resource</a>
        `;
    });
};

const createNewPinnedTopic = (topic) => {
    let newTopic = `
    <li>
        <a href="/show-topic/${topic.id}"class="waves-effect">
            <i class="small material-icons prefix">format_list_bulleted</i>
            ${topic.name}
        </a>
    </li>
    `;
    return newTopic;
}

const appendTopic = (topic) => {
    let pinnedTopicsList = document.querySelector('#pinned-topics-list');
    pinnedTopicsList.innerHTML += createNewPinnedTopic(topic);
}


const unpinTopic = (event) => {
    event.preventDefault();
    let id = event.target.firstChild.nextElementSibling.nextElementSibling.value;
    console.log('id: ', id);
    let parentDiv = event.target.parentNode;

    let data = new FormData;

    data.append('_token', "{{csrf_token()}}");
    data.append('id', id);
    
    fetch('/unpin-topic', {
        method: 'POST',
        credentials: 'same-origin',
        mode: 'cors',
        body: data
    }).then(response => {
        return response.json();
    }).then(data => {
        parentDiv.innerHTML = '';
        parentDiv.innerHTML += `
        <form method="POST" onsubmit="pinTopic(event)">
            <input type="hidden" value="{{csrf_token()}}" />
            <input type="hidden" name="id" value="${id}" />
            <button type="submit" class="btn blue hoverable">Pin Topic</button>
        </form>
        <a href="/submit-resource/${id}" class="btn green hoverable">Submit Resource</a>
        `;
        clearPinnedTopics();
        listPinnedTopics(data);
    });
}

const clearPinnedTopics = () => {
    document.querySelector('#pinned-topics-list').innerHTML = '';
}

const listPinnedTopics = (topics) => {
    topics.forEach(topic => {
        let pinnedTopicsList = document.querySelector('#pinned-topics-list');
        pinnedTopicsList.innerHTML += createNewPinnedTopic(topic);
    });
}