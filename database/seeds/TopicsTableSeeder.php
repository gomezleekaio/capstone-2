<?php

use Illuminate\Database\Seeder;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('topics')->insert([
            [
                'name' => 'AngularJS', 
                'description' => 
                'AngularJS is a JavaScript framework.',
                'image' => 'images/topics/angular.png',
                'status_id' => 1 
            ],
            [
                'name' => 'ReactJS', 
                'description' => 'React is a JavaScript library for building user interfaces.', 
                'image' => 'images/topics/react.jpeg',
                'status_id' => 1 
            ],
            [
                'name' => 'NodeJS', 
                'description' => 'Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient, perfect for data-intensive real-time applications that run across distributed devices.', 
                'image' => 'images/topics/node.png',
                'status_id' => 1
            ]
        ]);
    }
}
