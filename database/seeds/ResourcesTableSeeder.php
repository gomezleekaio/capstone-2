<?php

use Illuminate\Database\Seeder;

class ResourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resources')->insert([
            [
                'title' => 'Angular Official Documentation', 
                'url' => 'https://angular.io/docs',
                'user_id' => 1,
                'topic_id' => 1,
                'status_id' => 2
            ],
            [
                'title' => 'Angular w3schools', 
                'url' => 'https://www.w3schools.com/angular/',
                'user_id' => 1,
                'topic_id' => 1,
                'status_id' => 2
            ],
            [
                'title' => 'React Official Documentationb', 
                'url' => 'https://reactjs.org/docs/getting-started.html',
                'user_id' => 1,
                'topic_id' => 2,
                'status_id' => 2
            ],
            [
                'title' => 'Devdocs React', 
                'url' => 'https://devdocs.io/react/',
                'user_id' => 1,
                'topic_id' => 2,
                'status_id' => 2
            ]
        ]);
    }
}
