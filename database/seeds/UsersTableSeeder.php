<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
        		'email' => 'user@user.com',
        		'firstname' => 'Lee',
        		'lastname' => 'Gomez',
        		'password' => '$2y$10$Dxo.ASDkcm8.dWNCbETTC.eQ01wqk04luDveAB7S9WAgytoPrZVj2',
        		'image' => 'images/users/no-image.png',
        		'role_id' => 2,
        	],
        	[
        		'email' => 'admin@admin.com',
        		'firstname' => 'Ivana',
        		'lastname' => 'Alawi',
        		'password' => '$2y$10$Dxo.ASDkcm8.dWNCbETTC.eQ01wqk04luDveAB7S9WAgytoPrZVj2',
        		'image' => 'images/users/no-image.png',
        		'role_id' => 1,
        	],
        ]);
    }
}
