<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Topic extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function resources()
    {
        return $this->hasMany('App\Resource');
    }

    public function pinned()
    {
        $user = Auth::user();
        $pinner = $user->topics()->where('topic_id', $this->id)->first();
        if($pinner != null)
            return true;
        return false;
    }
}
