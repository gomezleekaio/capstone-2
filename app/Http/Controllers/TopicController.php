<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;
use App\User;
use Auth;

class TopicController extends Controller
{
    public function listTopics()
    {
        $topics = Topic::where('status_id', 2)->get();
        return view('admin-topics', compact('topics'));
    }

    public function create()
    {
        return view('add-topic');
    }

    public function store(Request $request)
    {

        $request->validate([
            'image' => 'required|image|mimes:jpeg,jpg,png,svg'
        ]);

        $topic = new Topic;
        $topic->name = $request->get('topic-name');
        $topic->description = $request->get('topic-description');
    
        $image = $request->file('image');
        $extension = $image->getClientOriginalExtension();
        $imageName = time().".".$extension;
        $destination = 'images/topics/';

        $image->move($destination, $imageName);

        $topic->image = $destination.$imageName;
        $topic->status_id = 2;
        $topic->save();

        return redirect('/admin/topics');
    }

    public function edit($id)
    {
        $user = Auth::user();
        $topic = Topic::find($id);
        return view('admin-edit-topic', compact('user','topic'));
    }

    public function update($id, Request $request)
    {
        $topic = Topic::find($id);

        $rules = [
            'topic-name' => 'required',
            'topic-description' => 'required',
            'image' => 'image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ];

        $this->validate($request, $rules);

        $topic->name = $request->get('topic-name');
        $topic->description = $request->get('topic-description');

        if($request->file('image') != null) {
            $image = $request->file('image');
            $image_name = time().".".$image->getClientOriginalExtension();

            $destination = 'images/';
            $image->move($destination, $image_name);

            $topic->image = $destination.$image_name;
        }

        $topic->save();

        return redirect('/admin/topics');
    }

    public function listSuggestedTopics()
    {
        $topics = Topic::where('status_id', 1)->get();
        return view('admin-suggested-topics', compact('topics'));
    }

    public function destroy($id)
    {
        Topic::destroy($id);
        return redirect('/admin/topics');
    }

    public function approveTopic($id)
    {
        $topic = Topic::find($id);
        $topic->status_id = 2;
        $topic->save();
        return redirect('/admin/suggested-topics');
    }

    public function pinTopic(Request $request)
    {
        $topic = Topic::find($request->get('id'));
        $user = Auth::user();
        $user->topics()->attach($topic->id);
        return response()->json($topic);
    }

    public function unpinTopic(Request $request)
    {
        $topic = Topic::find($request->get('id'));
        $user = Auth::user();
        $user->topics()->detach($request->get('id'));
        $topics = $user->topics;
        return response()->json($topics);
    }

    public function showTopic($id)
    {
        $topic = Topic::find($id);
        $resources = $topic->resources()->where('status_id', 2)->get();
        return view('topic', compact('topic', 'resources'));
    }

    public function showSuggestTopicForm()
    {
        return view('suggest-topic');
    }

    public function suggestTopic(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,jpg,png,svg'
        ]);

        $topic = new Topic;
        $topic->name = $request->get('topic-name');
        $topic->description = $request->get('topic-description');

        $image = $request->file('image');
        $extension = $image->getClientOriginalExtension();
        $imageName = time().".".$extension;
        $destination = 'images/topics/';

        $image->move($destination, $imageName);

        $topic->image = $destination.$imageName;
        $topic->status_id = 1;
        $topic->save();

        return redirect()->back();
    }

    public function searchTopic(Request $request)
    {
        $topic_to_search = $request->get('topic');
        $topics = Topic::where('name', 'like', '%'.$topic_to_search.'%')->get();
        return response()->json($topics);
    }
}
