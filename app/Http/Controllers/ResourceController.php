<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use App\Topic;
use Auth;

class ResourceController extends Controller
{
    public function create($id)
    {
        $user = Auth::user();
        $topic = Topic::find($id);

        return view('add-resource', compact('user', 'topic'));
    }

    public function store($id, Request $request)
    {

        $request->validate([
            'title' => 'required',
            'url' => 'required|url'
        ]);

        $resource = new Resource;
        $resource->title = $request->get('title');
        $resource->url = $request->get('url');
        $resource->user_id = Auth::user()->id;
        $resource->topic_id = $id;

        if (Auth::user()->role_id == 1) {
            $resource->status_id = 2;
        } else {
            $resource->status_id = 1;
        }
        
        $resource->save();

        if (Auth::user()->role_id == 1) {
            return redirect('/admin/resources');
        } else {
            return redirect()->back();
        }
    }

    public function listResources()
    {
        $user = Auth::user();
        $resources = Resource::where('status_id', 2)->get();
        return view('admin-resources', compact('user', 'resources'));
    }

    public function edit($id)
    {
        $resource = Resource::find($id);
        return view('admin-edit-resource', compact('resource'));
    }

    public function update($id, Request $request)
    {
        $resource = Resource::find($id);
        $resource->title = $request->get('title');
        $resource->url = $request->get('url');
        $resource->save();
        return redirect('/admin/resources');
    }

    public function destroy($id)
    {
        Resource::destroy($id);
        return redirect('/admin/submitted-resources');
    }

    public function listSubmittedResources()
    {
        $resources = Resource::where('status_id', 1)->get();
        return view('admin-submitted-resources', compact('resources'));
    }

    public function approveResource($id)
    {
        $resource = Resource::find($id);
        $resource->status_id = 2;
        $resource->save();
        return redirect('/admin/submitted-resources');
    }

    public function showSubmitResourceForm($id)
    {
        $topic = Topic::find($id);
        return view('submit-resource', compact('topic'));
    }
}
